#include <ESP8266WiFi.h>
#include <ThingSpeak.h>
//#include "secrets.h"

WiFiClient  client;

ADC_MODE(ADC_VCC);

#include <DHT.h>
#define DHTTYPE DHT22
#define DHTPIN 2
String LINE_TOKEN = "xxxxxx";
const char* ssid = "IT-RICD";
const char* password = "";
const char* DV_ID="T001";
//const char* servers="api.thingspeak.com";
const char* P_ID="xxxxxx";
const unsigned long CHNo=xxxxxx;
float t=0.0,h=0.0;
//const int sleepTimeS = 60;
const int sleepTimeS = 1800;
int i=0;

DHT dht(DHTPIN, DHTTYPE);

void setup() 
{
  Serial.begin(9600);
  delay(10);
  Serial.println(WiFi.macAddress());
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(1000);
    Serial.print(".");
    i++;
    if(i>=20)ESP.reset();
  }
  Serial.println("WiFi connected");
  Serial.println(WiFi.localIP() );

  ThingSpeak.begin(client);
  delay(1000);
  pinMode(DHTPIN,INPUT_PULLUP);
  
}

void loop()
{
  dht.begin();
  delay(100);
  h = dht.readHumidity();
  t = dht.readTemperature();
  if(t>=27.0){
      //Line_Notify(LINE_TOKEN, "SERVER Room too Hot"+String(t));
      Line_Notify(LINE_TOKEN, "อุณหภูมิห้อง Server RICD "+String(t));      
    }
  if (isnan(h) || isnan(t) ) 
  {
      Serial.println("Failed to read from DHT sensor!");
  }else{
    ThingSpeak.setField(1,h);
    ThingSpeak.setField(2,t);
    ThingSpeak.setField(3,ESP.getVcc());
    ThingSpeak.setField(4,1);
    ThingSpeak.writeFields(CHNo, P_ID);
    Serial.println(h);
    Serial.println(t);
    Serial.println(ESP.getVcc());
  }
  Serial.println("Deep Sleep Bye Bye");
  WiFi.forceSleepBegin();
  delay(sleepTimeS*1000);
  if (WiFi.status() != WL_CONNECTED)
  {
    ESP.reset();
  }
}
void Line_Notify(String LINE_Token, String message) {

  String msg = String("message=") + message;

  WiFiClientSecure client;
  if (!client.connect("notify-api.line.me", 443)) {
    Serial.println("connection failed");
    return;
  }

  String req = "";
  req += "POST /api/notify HTTP/1.1\r\n";
  req += "Host: notify-api.line.me\r\n";
  req += "Content-Type: application/x-www-form-urlencoded\r\n";
  req += "Authorization: Bearer " + String(LINE_Token) + "\r\n";
  req += "Content-Length: " + String(msg.length()) + "\r\n";
  req += "\r\n";
  req +=  msg;

  client.print(req);

  unsigned long timeout = millis();
  while (client.available() == 0) {
    if (millis() - timeout > 5000) {
      Serial.println(">>> Client Timeout !");
      client.stop();
      return;
    }
  }
}
