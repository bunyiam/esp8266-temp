#include <ESP8266WiFi.h>
#include <ThingSpeak.h>

WiFiClient  client;

ADC_MODE(ADC_VCC);

#include <DHT.h>
#define DHTTYPE DHT22
#define DHTPIN 2

const char* ssid = "SMART";
const char* password = "888888888";
const char* DV_ID="T001";
const char* servers="api.thingspeak.com";
const char* P_ID="G3NOJP2S812QJG6P";
const unsigned long CHNo=477455;
float t=0.0,h=0.0;
const int sleepTimeS = 60;
int i=0;

DHT dht(DHTPIN, DHTTYPE);

void setup() 
{
  Serial.begin(9600);
  delay(10);
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(1000);
    Serial.print(".");
    i++;
    if(i>=20)ESP.reset();
  }
  Serial.println("WiFi connected");
  Serial.println(WiFi.localIP() );

  ThingSpeak.begin(client);
  delay(1000);
  pinMode(DHTPIN,INPUT_PULLUP);
  
}

void loop()
{
  dht.begin();
  delay(100);
  h = dht.readHumidity();
  t = dht.readTemperature();
  if (isnan(h) || isnan(t) ) 
  {
      Serial.println("Failed to read from DHT sensor!");
  }else{
    ThingSpeak.setField(1,h);
    ThingSpeak.setField(2,t);
    ThingSpeak.setField(3,ESP.getVcc());
    ThingSpeak.setField(4,1);
    ThingSpeak.writeFields(CHNo, P_ID);
    Serial.println(h);
    Serial.println(t);
    Serial.println(ESP.getVcc());
  }
  Serial.println("Deep Sleep Bye Bye");
  WiFi.forceSleepBegin();
  delay(sleepTimeS*1000);
  if (WiFi.status() != WL_CONNECTED)
  {
    ESP.reset();
  }
}



